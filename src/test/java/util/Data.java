package util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.LockSupport;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Data {

    public static List<Integer> orderedIntegerList(final int size){
        final IntStream ints = IntStream.range(1, size + 1);
        final Stream<Integer> integers = ints.mapToObj(Integer::new);
        return integers.collect(Collectors.toList());
    }

}
