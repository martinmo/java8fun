package util;

import java.util.concurrent.atomic.AtomicInteger;

public class Computation implements Runnable{

    final static AtomicInteger computations = new AtomicInteger();

    final int name = computations.incrementAndGet();

    public Computation(){

    }

    @Override
    public void run() {
        _compute(5);
    }

    private void _compute(final int seconds){
        System.out.println("starting computation " + name + "      on thread " + Thread.currentThread().getName());
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("finished computation     " + name);
    }

    public static void compute(final int seconds){
        new Computation()._compute(seconds);
    }

    public static void reset(){
        System.out.println("computation RESET");
        computations.set(0);
    }
}
