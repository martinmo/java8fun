package util;

public class Timer {

    public final long start = System.currentTimeMillis();

    public int seconds(){
        return (int)(System.currentTimeMillis() - start) / 1000;
    }

    public void printSeconds(){
        System.out.println("TIME: " + (System.currentTimeMillis() - start) + "ms");
    }

}
