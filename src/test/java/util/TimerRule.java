package util;

import org.junit.rules.ExternalResource;

public class TimerRule extends ExternalResource{

    public Timer timer;

    @Override
    protected void before() throws Throwable {
        timer = new Timer();
    };

    @Override
    protected void after() {
        timer.printSeconds();
    };


}
