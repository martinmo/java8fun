package optional;

import org.junit.Test;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.assertFalse;

public class BasicOptionalTest {

    @Test
    public void basic() {
        final String string = "String";
        final Optional<String> option = Optional.of(string);
        assertTrue(option.isPresent());

        assertTrue(string.equals(option.get()));

        final Optional<String> fromNull = Optional.ofNullable(null);
        assertFalse(fromNull.isPresent());
    }


    @Test(expected = NullPointerException.class)
    public void nonNull() {
        final Optional<String> o2 = Optional.of(null);
    }

    @Test
    public void fallBack() {
        final Optional<Integer> empty = Optional.empty();

        final Integer orElse = empty.orElse(0);
        assertTrue(orElse == 0);

        final Integer orElseLazy = empty.orElseGet(() -> 0);
        assertTrue(orElseLazy == 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void fallBackWithException() {
        final Optional<Integer> empty = Optional.empty();
        empty.orElseThrow(IllegalArgumentException::new);
    }

    @Test
    public void filter() {
        final Optional<String> empty = Optional.of("");
        final Optional<String> filtered = empty.filter(s -> !s.isEmpty());

        assertFalse(filtered.isPresent());
    }

    @Test
    public void map() {
        final Optional<String> string = Optional.of("1");
        final Optional<Integer> integer = string.map(Integer::parseInt);
        assertTrue(integer.get() == 1);
    }

    @Test(expected = NumberFormatException.class)
    public void mapFail() {
        final Optional<String> string2 = Optional.of("a");
        final Optional<Integer> integer2 = string2.map(Integer::parseInt);
    }

    @Test
    // Optional<A>.flatMap<T>( A -> Optional<B> ) -> Optional<B>
    public void flatMap() {
        final Optional<String> string = Optional.of("a");
        final Optional<Integer> integer2 = string.flatMap(s -> {
            try {
                return Optional.of(Integer.parseInt(s));
            } catch (NumberFormatException e) {
                return Optional.empty();
            }
        });

        assertFalse(integer2.isPresent());


        Function<String, Optional<Integer>> intParser = (s) -> {
            try {
                return Optional.of(Integer.parseInt(s));
            } catch (NumberFormatException e) {
                return Optional.empty();
            }
        };

        assertFalse(string.flatMap(intParser).isPresent());

        assertTrue(Optional.of("1").flatMap(intParser).isPresent());

    }

    private Function<String, Optional<Integer>> integerParser = (s) -> {
        try {
            return Optional.of(Integer.parseInt(s));
        } catch (Exception e) {
            return Optional.empty();
        }
    };

    @Test
    public void streamIt() {
        final Stream<String> strings = Stream.of("1", "a", "", "sadf", "2");
        final Stream<Optional<Integer>> optionalIntegers = strings.map(integerParser);
        final Stream<Integer> integers = optionalIntegers.filter(Optional::isPresent).map(Optional::get);
        assertTrue(integers.collect(Collectors.toList()).size() == 2);
    }
}
