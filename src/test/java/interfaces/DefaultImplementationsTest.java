package interfaces;

import org.junit.Test;

public class DefaultImplementationsTest {

    private static interface Printer {
        default void print(){
            System.out.println("printer");
        }
    }

    private static class PlainPrinter implements Printer{};

    private static class LaserPrinter implements Printer{
        @Override public void print(){
            System.out.println("laser");
        }
    }

    private static interface FastPrinter{
        default void print(){
            System.out.println("fast");
        }
    }

    private static class FastPrinterImpl implements Printer, FastPrinter{
        @Override public void print(){ // necessary!
            Printer.super.print();
            FastPrinter.super.print();
        }
    }

    private static class FastLaserPrinter extends LaserPrinter implements FastPrinter{}

    @Test
    public void print(){

        new Printer(){}.print();
        System.out.println();

        new PlainPrinter(){}.print();
        System.out.println();

        new LaserPrinter().print();
        System.out.println();

        ((Printer)(new LaserPrinter())).print();
        System.out.println();

        new FastPrinterImpl().print();
        System.out.println();

        new FastLaserPrinter().print();
    }

}
