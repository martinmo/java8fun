package lambdas;

import org.junit.Test;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class ScopeTest {

    public final String s = "1";

    @Test
    public void whoIsThis(){

        final String s = "2";

        final String arg = "3";

        final Function<String, String> closure = (str) -> this.s + s + str;

        final String result = closure.apply(arg);

        assertThat(result, equalTo("123"));
    }

    @Test
    public void whoIsThis2(){

        final String arg = "3";

        final Function<String, String> closure = (str) -> this.s + s + str;

        final String result = closure.apply(arg);

        assertThat(result, equalTo("113"));
    }


    @Test
    public void anonymousClassScope(){

        final String s = "2";

        final String arg = "3";

        final Function<String, String> anonymousClassClosure = new Function<String, String>() {

            final String s = "4";

            @Override
            public String apply(String s) {

                return  s + this.s;
            }
        };

        final String result = anonymousClassClosure.apply(arg);

        assertThat(result, equalTo("34"));

        final Function<String, String> lambda = (a2) -> {
            //String s = 4; // does not compile: already defined in scope
            return this.s + s + a2;
        };

        assertThat(lambda.apply("3"), equalTo("123"));
    }

}
