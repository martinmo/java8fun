package lambdas;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import util.Data;

import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertThat;

public class BasicLambdaTest {

    @Test
    public void simple() {

        final Comparator<Integer> integerComparator = (a, b) -> b.compareTo(a);

        final List<Integer> ints = Data.orderedIntegerList(3);

        ints.sort(integerComparator);

        assertTrue(ints.get(0) == 3);
    }

    @Test
    public void oldSchool() {
        final Comparator<Integer> integerComparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer a, Integer b) {
                return b.compareTo(a);
            }
        };
        final List<Integer> ints = Data.orderedIntegerList(3);
        ints.sort(integerComparator);
        assertTrue(ints.get(0) == 3);
    }

    @Test
    public void simpleWithReference() {

        final List<Integer> ints = Data.orderedIntegerList(3);

        ints.sort(Integer::compare);

        assertTrue(ints.get(0) == 1);
    }


    public static class Printer {
        int times = 1;

        public Printer() {
        }

        public Printer(final Object o) {
            System.out.println("from constructor");
            print(o);
        }

        public void print(final Object o) {
            System.out.println("printing for the " + times + " time");
            times++;
            System.out.println(o);
        }

        public static void printStatic(final Object o) {
            new Printer().print(o);
        }
    }

    @Test
    public void moreOnReferences() {

        final List<Integer> ints = Data.orderedIntegerList(1);

        // static method
        ints.stream().forEach(System.out::println);
        ints.stream().forEach(Printer::printStatic);
        ints.stream().forEach((o) -> Printer.printStatic(o));

        // instance method

        //      self
        ints.stream().forEach(Object::hashCode);

        //      other instance
        final Printer printer = new Printer();
        ints.stream().forEach(o -> printer.print(o));
        ints.stream().forEach(printer::print);

        // constructor
        ints.stream().forEach(Printer::new);
    }

    @Test
    public void functions() {

        UnaryOperator<Integer> adder = (i) -> i + 1;
        UnaryOperator<Integer> adder1 = (i) -> {
            return i + 1;
        };
        UnaryOperator<Integer> adder2 = (Integer i) -> i + 1;

        Function<Integer, Integer> adder3 = (i) -> i + 1;

        BiFunction<Integer, Integer, Integer> add = (a, b) -> a + b;
    }

    @Test
    public void chainingFunction() {
        UnaryOperator<String> append1 = (s) -> s + "1";
        UnaryOperator<String> append2 = (s) -> s + "2";
        UnaryOperator<String> append3 = (s) -> s + "3";

        final Function<String, String> andThen = append1.andThen(append2).andThen(append3);
        assertThat(andThen.apply("A"), CoreMatchers.equalTo("A123"));

        final Function<String, String> composed = append1.compose(append2).compose(append3);
        assertThat(composed.apply("A"), CoreMatchers.equalTo("A321"));
    }

    private static void boom(final String who) {
        System.out.println("********** BOOOOOOM " + who + " **********");
    }

    private static String decorade(final String str) {
        return "----> " + str + " <----";
    }

    @Test
    public void goingLazy() { // passing references
        boom("you");

        Consumer<String> boomer = (String who) -> boom(who);
        System.out.println("want some boomer? here you go:");
        boomer.accept("me");

        UnaryOperator<String> decorator = BasicLambdaTest::decorade;
        System.out.println(decorator.apply("PEACE"));
    }
}
