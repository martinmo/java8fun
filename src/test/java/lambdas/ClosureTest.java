package lambdas;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ClosureTest {

    @Test
    public void closeOverState(){

        final List<Supplier<Integer>> suppliers = new ArrayList<>();

        for(int i = 1; i < 4; i++){
            final int j = i;
            suppliers.add(() -> j);
        }

        final List<Integer> integers = suppliers.stream().map(Supplier::get).collect(Collectors.toList());

        assertThat(integers, equalTo(Arrays.asList(1,2,3)));
    }

    @Test
    public void closeOverMutableState(){
        final AtomicInteger counter = new AtomicInteger(0);

        Supplier<Integer> c1 = () -> counter.get();

        Supplier<Integer> c2 = new Supplier<Integer>() {
            private final int i = counter.get();
            @Override public Integer get() {return i;}
        };

        assertThat(c1.get(), equalTo(0));
        assertThat(c2.get(), equalTo(0));

        counter.incrementAndGet();
        assertThat(c1.get(), equalTo(1));
        assertThat(c2.get(), equalTo(0));
    }

    private int i = 0;

    public int getInt(){return i;}

    @Test
    public void closeOverSelf(){
        Supplier<Integer> in = this::getInt;
        Supplier<Integer> in2 = () -> i; // i is not final?

        assertThat(in.get(), equalTo(0));
        assertThat(in2.get(), equalTo(0));
        i++;
        assertThat(in.get(), equalTo(1));
        assertThat(in2.get(), equalTo(1));
    }

}
