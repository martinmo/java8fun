package lambdas;

import java.util.Date;
import java.util.function.*;

public class Functions {

    // (a) -> ()
    Consumer<Integer> onlySideEffects = i -> System.out.println(i);

    // () -> (a)
    Supplier<Long> now = () -> new Date().getTime();

    // (a) -> (a)
    UnaryOperator<Integer> addOne = i -> i + 1;

    // (a) -> (b)
    Function<Integer, String> toStr = i -> String.valueOf(i);

    // (a,b) -> (c)
    BiFunction<Integer, String, StringBuffer> buffer = (i,s) -> new StringBuffer().append(i).append(s);

    // (a) -> Bool
    Predicate<Integer> isMoreThanTen = i -> i > 10;

    // (a,b) -> Bool
    BiPredicate<String, String> checkUsernamePassword = (username, password) -> username.equals(password);

    // (a,a) -> (a)
    BinaryOperator<Integer> add = (i1, i2) -> i1 + i2;
}
