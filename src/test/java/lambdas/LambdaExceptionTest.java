package lambdas;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Function;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class LambdaExceptionTest {

    @Test(expected = NumberFormatException.class)
    public void runtimeExceptionsCanBeCatchedByTheCaller() {
        Function<String, Integer> toInt = s -> Integer.parseInt(s);

        toInt.apply("a");
    }


    // http://www.tutego.de/blog/javainsel/2014/03/java-8-ausnahme-lambda-ausdruecke/
    private static interface UnsafePredicate<A> {
        boolean test(A a) throws Throwable;
    }

    @Test
    public void lambdasCanImplementExceptionThrowingMethods() {
        UnsafePredicate<Path> isFileEmpty = path -> Files.size(path) > 0;

        try {
            assertThat(isFileEmpty.test(Paths.get("nonexsitent")), is(false));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        // problem: every caller must catch this
    }
}
