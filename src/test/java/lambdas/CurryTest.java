package lambdas;

import org.junit.Test;

import java.util.function.BiFunction;
import java.util.function.Function;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CurryTest {

    @Test
    public void curryExampels() {
        BiFunction<Integer, Integer, Integer> adder = (i, j) -> i + j;
        assertThat(adder.apply(1, 1), equalTo(2));

        Function<Integer, Function<Integer, Integer>> curriedAdder = (i) -> (j) -> adder.apply(i, j);

        final Function<Integer, Integer> partialAppliedAdder = curriedAdder.apply(1);

        assertThat(partialAppliedAdder.apply(1), equalTo(2));
    }

    private static <A, B, C> Function<A, Function<B, C>> curry(BiFunction<A, B, C> f) {
        return a -> b -> f.apply(a, b);
    }

    @Test
    public void curringAFunction() {
        BiFunction<Integer, Integer, Integer> adder = (i, j) -> i + j;

        final Function<Integer, Function<Integer, Integer>> curry = curry(adder);

        final Function<Integer, Integer> withFirstArgument = curry.apply(1);

        final int result = withFirstArgument.apply(2);

        assertThat(result, is(3));
    }

    @Test
    public void partialApplication() {
        BiFunction<Integer, Integer, Integer> adder = (i, j) -> i + j;
        Function<Integer, Integer> incrementer = i -> adder.apply(i, 1);

        assertThat(incrementer.apply(5), equalTo(6));
    }

    private static class Rice {
        final String name = "rice";
    }

    private static class Curry {
        final String name;

        private Curry(String name) {
            this.name = name;
        }

        public static Curry red() {
            return new Curry("red curry");
        }

        public static Curry green() {
            return new Curry("green curry");
        }
    }

    private static class Meal {
        final Rice rice;
        final Curry curry;

        public Meal(Rice rice, Curry curry) {
            this.rice = rice;
            this.curry = curry;
        }

        @Override
        public String toString() {
            return "Tasty meal of " + rice.name + " with " + curry.name;
        }
    }

    @Test
    public void makeCurry() {
        BiFunction<Rice, Curry, Meal> makeCurry = (rice, curry) -> new Meal(rice, curry);

        Function<Curry, Meal> partiallyMadeMeal = (curry) -> makeCurry.apply(new Rice(), curry);

        final Meal greenCurryMeal = partiallyMadeMeal.apply(Curry.green());
        assertThat(greenCurryMeal.toString(), equalTo("Tasty meal of rice with green curry"));

        final Meal redCurryMeal = partiallyMadeMeal.apply(Curry.red());
        assertThat(redCurryMeal.toString(), equalTo("Tasty meal of rice with red curry"));
    }


}
