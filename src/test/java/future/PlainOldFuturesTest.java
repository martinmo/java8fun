package future;

import org.junit.Test;

import java.util.concurrent.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PlainOldFuturesTest {

    ExecutorService executor = Executors.newFixedThreadPool(10);
//    ExecutorService executor = new ForkJoinPool(10);

    Callable<String> callable = () -> {
        System.out.println("called on thread " + Thread.currentThread().getName());
        Thread.sleep(1000);
        return "done";
    };

    Callable<String> failingCallable = () -> {
        System.out.println("failing called on thread " + Thread.currentThread().getName());
        throw new Exception("failed to call");
    };

    private static void printThreadName() {
        System.out.println("TN: " + Thread.currentThread().getName());
    }

    @Test
    public void plainOldFuture() throws ExecutionException, InterruptedException {
        printThreadName();

        final Future<String> result = executor.submit(callable);

        assertThat(result.isDone(), is(false)); // no guaranties here

        final String resultUnwrapped = result.get(); //block
        assertThat(result.isDone(), is(true));
        System.out.println("result: " + resultUnwrapped);
        printThreadName();
    }

    @Test
    public void plainOldFutures() throws ExecutionException, InterruptedException {
        printThreadName();

        final Stream<Future<String>> futureStream = IntStream.rangeClosed(1, 2).boxed()
                .map(i -> executor.submit(callable));

        // futureStream.map(f -> f.get())  !! Exception Handling
        final Stream<String> fullFilledFutures = futureStream.flatMap(f -> {
            try {
                return Stream.of(f.get()); // block
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return Stream.<String>empty();
            }
        });

        assertThat(fullFilledFutures.count(), is(2L));
        printThreadName();
    }

    @Test
    public void failingPlainOldFutures() {
        printThreadName();

        final Stream<Future<String>> futureStream = IntStream.rangeClosed(1, 2).boxed()
                .map(i -> executor.submit(failingCallable));

        // futureStream.map(f -> f.get())  !! Exception Handling
        final Stream<String> fullFilledFutures = futureStream.flatMap(f -> {
            try {
                return Stream.of(f.get());
            } catch (InterruptedException | ExecutionException e) {
                System.err.println("execution failed");
                return Stream.<String>empty();
            }
        });


        assertThat(fullFilledFutures.count(), is(0L));
        printThreadName();
    }


}
