package future;

import org.junit.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class DoItYourselfCompletableFuturesTest {

    private static class Computation implements Runnable {

        CompletableFuture<String> result = new CompletableFuture<>();

        public CompletableFuture<String> getResult() {
            return result;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(1000);
                result.complete("done");
            } catch (InterruptedException e) {
                e.printStackTrace();
                result.completeExceptionally(e);
            }
        }
    }


    @Test
    public void complete() throws ExecutionException, InterruptedException {
        final Computation computation = new Computation();
        final CompletableFuture<String> result = computation.getResult();

        assertThat(result.isDone(), is(false));

        Executors.newFixedThreadPool(1).submit(computation);

        // no guarantees
        assertThat(result.isDone(), is(false));

        assertThat(result.get(), equalTo("done"));

    }

}
