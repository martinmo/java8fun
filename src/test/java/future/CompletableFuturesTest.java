package future;

import org.junit.Test;

import java.util.concurrent.*;
import java.util.function.Supplier;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class CompletableFuturesTest {

    ExecutorService executorService = Executors.newFixedThreadPool(10);

    Supplier<String> computation = () -> {
        System.out.println("called on thread " + Thread.currentThread().getName());
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "done";
    };


    @Test
    public void completeAndThen() throws ExecutionException, InterruptedException {

        System.out.println("Thread: " + Thread.currentThread().getName());

        final CompletableFuture<String> futureA = CompletableFuture.supplyAsync(computation);
        final CompletableFuture<String> futureB = CompletableFuture.supplyAsync(computation);


        final CompletableFuture<String> futureA2 = futureA.whenComplete((result, exception) -> {
            System.out.println("completed on thread " + Thread.currentThread().getName());
        });

        final CompletableFuture<String> futureB2 = futureB.whenComplete((result, exception) -> {
            System.out.println("completed on thread " + Thread.currentThread().getName());
        });

        futureA2.get();
        futureB2.get();
    }

    @Test
    public void completeAndThenAsync() throws ExecutionException, InterruptedException {

        System.out.println("Thread: " + Thread.currentThread().getName());

        final CompletableFuture<String> futureA = CompletableFuture.supplyAsync(computation, executorService);
        final CompletableFuture<String> futureB = CompletableFuture.supplyAsync(computation, executorService);


        final CompletableFuture<String> futureA2 = futureA.whenCompleteAsync((result, exception) -> {
            System.out.println("completed on thread " + Thread.currentThread().getName());
        }, executorService);

        final CompletableFuture<String> futureB2 = futureB.whenCompleteAsync((result, exception) -> {
            System.out.println("completed on thread " + Thread.currentThread().getName());
        }, executorService);

        futureA2.get();
        futureB2.get();
    }

    @Test
    public void completeAndThenCompose() throws ExecutionException, InterruptedException {

        System.out.println("Thread: " + Thread.currentThread().getName());

        final CompletableFuture<String> futureA = CompletableFuture.supplyAsync(computation);
        final CompletableFuture<String> futureB = CompletableFuture.supplyAsync(computation);

        final CompletableFuture<String> futureA2 = futureA.thenCombine(futureB, (s1, s2) -> {
            System.out.println("combining on thread " + Thread.currentThread().getName());
            return s1 + " and " + s2;
        });

        assertThat(futureA2.get(), equalTo("done and done"));
    }

}
