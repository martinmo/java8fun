package examples.db;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.function.Function;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DbTest {

    private DB db;

    @Before
    public void setup() {
        db = DB.inMemDB();
        db.tx( c -> {
            c.createStatement().executeUpdate( "CREATE TABLE test ( num INT)" );

            final PreparedStatement preparedStatement = c.prepareStatement( "INSERT INTO test values (?)" );
            preparedStatement.setInt( 1, 1 );

            return preparedStatement.execute();
        } );
    }

    @After
    public void stop() {
        db.tx( c -> c.createStatement().executeUpdate( "DROP TABLE test" ) );
    }


    @Test
    public void canUseConnection() {

        long result = db.tx( c -> {
            final ResultSet resultSet = c.createStatement().executeQuery( "SELECT num FROM test LIMIT 1" );
            resultSet.next();
            return resultSet.getLong( 1 );
        } );
        assertThat( result, is( 1L ) );
    }

    private final static SQLFunction<ResultSet, Integer> COLUMN_MAPPER = rs -> {
        final Integer text1 = rs.getInt( "num" );
        return text1;
    };

    @Test
    public void canUseSelect() {

        final List<Integer> result = db.tx( c -> {
            final ResultSet rs = DB.select( c, "SELECT num FROM test WHERE num = ?", 1 );

            return DB.map( rs, COLUMN_MAPPER );
        } );
        assertThat( result, CoreMatchers.hasItem( 1 ) );
    }

    @Test
    public void canUseSelectWithMapper() {

        final List<Integer> result = db.tx( c -> DB.select( c, COLUMN_MAPPER, "SELECT num FROM test WHERE num = ?", 1 ) );

        assertThat( result, CoreMatchers.hasItem( 1 ) );
    }

    @Test
    public void canUseSelectWithMap() {

        final List<Integer> result = db.tx( c ->
                                                    DB.map( DB.select( c, "SELECT num FROM test WHERE num = ?", 1 ), COLUMN_MAPPER ) );

        assertThat( result, CoreMatchers.hasItem( 1 ) );
    }

    @Test
    public void canUseSelectWithResultWrapper() {

        final List<Integer> result = db.tx( c -> {

            /**
             *  return DB.result(DB.select(c, "SELECT num FROM test WHERE num = ?", new Integer(1))).apply(COLUMN_MAPPER);
             *  does not compile because of type erasure
             */

            final Function<SQLFunction<ResultSet, Integer>, List<Integer>> resultF = DB
                    .result( DB.select( c, "SELECT num FROM test WHERE num = ?", 1 ) );
            return resultF.apply( COLUMN_MAPPER );
        } );
        assertThat( result, CoreMatchers.hasItem( 1 ) );
    }

    @Test
    public void isTransactional(){

        final Long count = db.tx( c -> {
            final ResultSet resultSet = c.createStatement().executeQuery( "select count(*) from test" );
            resultSet.next();
            return resultSet.getLong( 1 );
        } );

        assertThat( count, is(1L) );

        try {
            db.tx( c -> {
                c.createStatement().executeUpdate( "DELETE FROM test" );
                throw new RuntimeException( "rollback" );
            } );
        }catch ( Throwable t ){
            System.out.println("Ignoring exception");
            t.printStackTrace();
        }

        final Long countAfter = db.tx( c -> {
            final ResultSet resultSet = c.createStatement().executeQuery( "select count(*) from test" );
            resultSet.next();
            return resultSet.getLong( 1 );
        } );

        assertThat( countAfter, is(1L) );
    }
}
