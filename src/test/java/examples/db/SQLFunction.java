package examples.db;

import java.sql.SQLException;

public interface SQLFunction<E,T> {
    T apply( E e ) throws SQLException;
}
