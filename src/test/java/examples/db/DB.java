package examples.db;

import org.hsqldb.jdbc.JDBCDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;


public class DB {

    private final static Logger LOG = LoggerFactory.getLogger(DB.class);

    protected final DataSource dataSource;

    public DB(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static DB inMemDB() {
        JDBCDataSource dataSource = new JDBCDataSource();

        dataSource.setUrl("jdbc:hsqldb:mem:inMemDB");

        return new DB(dataSource);
    }

    public <T> T tx(SQLFunction<Connection, T> f) {
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            try {
                final T result = f.apply(connection);
                connection.commit();
                return result;
            } catch (Exception e) { // rollback on any exception
                connection.rollback();
                throw new RuntimeException(e);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static ResultSet select(Connection con, String query, Object... args) {
        try {
            final PreparedStatement statement = con.prepareStatement(query);
            for (int i = 0; i < args.length; i++) {
                statement.setObject(i + 1, args[i]);
            }
            return statement.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> List<T> select(Connection con, SQLFunction<ResultSet, T> f, String query, Object... args) {
        final ResultSet resultSet = select(con, query, args);
        return map(resultSet, f);
    }

    public static <T> List<T> map(ResultSet rs, SQLFunction<ResultSet, T> f) {
        try {
            final List<T> result = new LinkedList<>();
            while (rs.next()){
                result.add(f.apply(rs));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // not useful
    public static <T> Function<SQLFunction<ResultSet, T>, List<T>> result(ResultSet rs){
        return f -> map(rs, f);
    }

}
