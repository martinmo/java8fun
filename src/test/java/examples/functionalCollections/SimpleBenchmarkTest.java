package examples.functionalCollections;

import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.carrotsearch.junitbenchmarks.BenchmarkRule;
import com.google.common.collect.FluentIterable;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static junit.framework.Assert.assertTrue;

@BenchmarkOptions(benchmarkRounds = 5000, warmupRounds = 1000)
public class SimpleBenchmarkTest {

    private static final int reps = 500;
    private static final int listSize = 100;

    @Rule
    public TestRule benchmarkRun = new BenchmarkRule();

    private static List<Integer> ints;

    @BeforeClass
    public static void setUp() throws Exception {
        ints = IntStream.range(0, listSize).boxed().collect(Collectors.toList());
    }

    @Test
    public void functionalList() {
        FunctionalList<Integer> filter;
        for (int r = 0; r < reps; r++) {
            filter = FunctionalList.decorate(ints).map(i -> i * 3);
            assertTrue(filter.size() == listSize);
        }
    }

    @Test
    public void functionalLinkedList() {
        FunctionalList<Integer> filter;
        for (int r = 0; r < reps; r++) {
            filter = FunctionalList.decorate(ints, LinkedList::new).map(i -> i * 3);
            assertTrue(filter.size() == listSize);
        }
    }

    @Test
    public void java8stream() {
        List<Integer> filter;
        for (int r = 0; r < reps; r++) {
            filter = ints.stream().map(i -> i * 3).collect(Collectors.toList());
            assertTrue(filter.size() == listSize);
        }
    }


    @Test
    public void java8parallelStream() {
        List<Integer> filter;
        for (int r = 0; r < reps; r++) {
            filter = ints.stream().parallel().map(i -> i * 3).collect(Collectors.toList());
            assertTrue(filter.size() == listSize);
        }
    }

    @Test
    public void functionalIteratorPlain() {
        FunctionalIterator<Integer> filter;
        for (int r = 0; r < reps; r++) {
            filter = FunctionalIterator.wrap(ints.iterator()).map(i -> i * 3);
            int size = 0;
            while (filter.hasNext()) {
                filter.next();
                size++;
            }
            assertTrue(size == listSize);
        }
    }

    @Test
    public void functionalIteratorToList() {
        FunctionalIterator<Integer> filter;
        for (int r = 0; r < reps; r++) {
            filter = FunctionalIterator.wrap(ints.iterator()).map(i -> i * 3);
            List<Integer> result = filter.toList();
            assertTrue(result.size() == listSize);
        }
    }

    @Test
    public void functionalIterator2ToList() {
        FunctionalIterator2<Integer> filter;
        for (int r = 0; r < reps; r++) {
            filter = FunctionalIterator2.wrap(ints.iterator()).map(i -> i * 3);
            List<Integer> result = filter.toList();
            assertTrue(result.size() == listSize);
        }
    }

    @Test
    public void fluentIterableToList() {
        for (int r = 0; r < reps; r++) {
            final FluentIterable<Integer> fluent = FluentIterable.from(ints).transform(i -> i * 3);
            List<Integer> result = fluent.toList();
            assertTrue(result.size() == listSize);
        }
    }
}
