package examples.functionalCollections;

import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Martin Moeller.
 * akquinet tech@spree GmbH
 */
public class FunctionalListTest {
    @Test
    public void canFold(){
        final FunctionalList<Integer> list = FunctionalList.decorate( Arrays.asList( 1, 2, 3 ) );

        final Integer sum = list.fold( 0, ( acc, elem ) -> acc + elem );

        assertThat(sum, equalTo(6));
    }


}
