package examples.functionalCollections;

import com.google.common.collect.ForwardingIterator;
import com.google.common.collect.Iterators;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class FunctionalIterator2<E> extends ForwardingIterator<E> {

    private final Iterator<E> underlying;

    private FunctionalIterator2(Iterator<E> underlying) {
        this.underlying = underlying;
    }

    public static <F> FunctionalIterator2<F> wrap(final Iterator<F> it) {
        return new FunctionalIterator2(it);
    }

    public static <F> FunctionalIterator2<F> from(final Iterable<F> iterable) {
        return new FunctionalIterator2(iterable.iterator());
    }

    @Override
    protected Iterator<E> delegate() {
        return underlying;
    }

    public <F> FunctionalIterator2<F> map(final Function<E, F> f) {
        return new FunctionalIterator2<>(Iterators.transform(underlying, e -> f.apply(e)));
    }

    public FunctionalIterator2<E> filter(final Predicate<E> f) {
        return new FunctionalIterator2<>(Iterators.filter(underlying, e -> f.test(e)));
    }

    public <F> FunctionalIterator2<F> flatMap(final Function<E, Iterator<F>> f) {
        return new FunctionalIterator2<>(Iterators.concat(this.map((E e) -> f.apply(e))));
    }

    public <F> FunctionalIterator2<F> flatMapCollection(final Function<E, Collection<F>> f) {
        return new FunctionalIterator2<>(Iterators.concat(this.map(e -> f.apply(e).iterator())));
    }

    public void forEach(Consumer<E> f) {
        this.forEachRemaining(f::accept);
    }

    public List<E> toList() {
        final List<E> list = new LinkedList<>();
        while (underlying.hasNext()) {
            list.add(underlying.next());
        }
        return list;
    }
}
