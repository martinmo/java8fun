package examples.functionalCollections;

import com.google.common.collect.ForwardingCollection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class FunctionalList<E> extends ForwardingCollection<E> {
    private final Collection<E> original;

    FunctionalList(Collection<E> original, Supplier<Collection<?>> listImpl) {
        this.listImpl = listImpl;
        this.original = original;
    }

    private final Supplier<Collection<?>> listImpl;

    public static <E> FunctionalList<E> decorate(final Collection<E> collection) {
        return new FunctionalList<>(collection, ArrayList::new);
    }

    public static <E> FunctionalList<E> decorate(final Collection<E> collection, Supplier<Collection<?>> listImpl) {
        return new FunctionalList<>(collection, listImpl);
    }

    @Override
    protected Collection<E> delegate() {
        return original;
    }

    public <F> FunctionalList<F> map(Function<E, F> f) {
        final FunctionalList<F> result = new FunctionalList(listImpl.get(), listImpl);

        // result.addAll(original.stream().map(f::apply).collect(Collectors.toList()));
        for (E e : original) {
            result.add(f.apply(e));
        }
        return result;
    }

    public FunctionalList<E> filter(Predicate<E> filter) {
        final Collection<E> result = (Collection<E>) listImpl.get();
        for (E e : original) {
            if (filter.test(e)) {
                result.add(e);
            }
        }
        return new FunctionalList(result, listImpl);
    }

    public <F> FunctionalList<F> flatMap(Function<E, Collection<F>> f) {
        final Collection<F> result = (Collection<F>) listImpl.get();
        for (E e : original) {
            result.addAll(f.apply(e));
        }
        return new FunctionalList(result, listImpl);
    }

    public <F> F fold(F init, BiFunction<F, E, F> f){

        F current = init;
        for(E e : original){
            current = f.apply( current, e );
        }
        return current;
    }
}
