package examples.functionalCollections;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.google.common.collect.Iterators.forArray;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class IteratorTest {

    @Test
    public void test() {
        final List<Integer> integers = Arrays.asList(1, 2, 3);
        final FunctionalIterator<Integer> functionalIterator = FunctionalIterator.wrap(integers.iterator());

        final FunctionalIterator<Integer> map = functionalIterator.map(i -> i + 1);

        List<Integer> result = Lists.newArrayList(map);

        assertThat(result, hasItems(2, 3, 4));
    }

    @Test
    public void testMapping() {
        final List<Integer> integers = Arrays.asList(1, 2, 3);
        final FunctionalIterator<Integer> functionalIterator = FunctionalIterator.wrap(integers.iterator());

        final FunctionalIterator<String> map = functionalIterator.map(i -> i + 1).map(i -> i + "");

        List<String> result = Lists.newArrayList(map);

        assertThat(result, hasItems("2", "3", "4"));
    }

    @Test
    public void testFilter() {
        final List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5);
        final FunctionalIterator<Integer> functionalIterator = FunctionalIterator.wrap(integers.iterator());

        final FunctionalIterator<Integer> map = functionalIterator.filter(i -> i % 2 == 0);

        List<Integer> result = Lists.newArrayList(map);

        assertThat(result.size(), is(2));
        assertThat(result, hasItems(2, 4));
    }

    @Test
    public void testLazy() {

        AtomicInteger done = new AtomicInteger(0);

        final List<Integer> integers = Arrays.asList(1, 2, 3);
        final FunctionalIterator<Integer> functionalIterator = FunctionalIterator.wrap(integers.iterator());

        final FunctionalIterator<Integer> map = functionalIterator.map(i -> {
            done.incrementAndGet();
            return i + 1;
        });

        assertThat(done.get(), equalTo(0));

        map.next();
        assertThat(done.get(), equalTo(1));

        map.next();
        assertThat(done.get(), equalTo(2));
    }

    @Test
    public void flatMapFlatMaps() {
        final List<String> stringsList = Arrays.asList("ab", "cd");
        FunctionalIterator2<String> strings = FunctionalIterator2.wrap(stringsList.iterator());

        final FunctionalIterator2<String> flat = strings.flatMap(s -> forArray(s.split("")));

        String next = flat.next();
        assertThat(next, equalTo("a"));

        next = flat.next();
        assertThat(next, equalTo("b"));

        next = flat.next();
        assertThat(next, equalTo("c"));

        next = flat.next();
        assertThat(next, equalTo("d"));

        assertThat(flat.hasNext(), is(false));
    }

    @Test
    public void flatMapCollections() {
        final List<String> stringsList = Arrays.asList("ab", "cd");
        FunctionalIterator2<String> strings = FunctionalIterator2.wrap(stringsList.iterator());

        final FunctionalIterator2<String> flat = strings.flatMapCollection(s -> Arrays.asList(s.split("")));

        String next = flat.next();
        assertThat(next, equalTo("a"));

        next = flat.next();
        assertThat(next, equalTo("b"));

        next = flat.next();
        assertThat(next, equalTo("c"));

        next = flat.next();
        assertThat(next, equalTo("d"));
    }

    @Test
    public void forEach() {
        final List<String> stringsList = Arrays.asList("a", "b", "c");
        FunctionalIterator2<String> strings = FunctionalIterator2.wrap(stringsList.iterator());

        final List<String> result = new LinkedList<>();

        strings.forEach(s -> result.add(s));

        assertThat(result, hasItems("a", "b", "c"));
    }

    @Test
    public void flatMapCollectionsToList() {
        final List<String> stringsList = Arrays.asList("ab", "cd");
        FunctionalIterator2<String> strings = FunctionalIterator2.wrap(stringsList.iterator());

        final FunctionalIterator2<String> flat = strings.flatMapCollection(s -> Arrays.asList(s.split("")));

        final List<String> result = flat.toList();

        assertThat(result, hasItems("a", "b", "c", "d"));
    }
}
