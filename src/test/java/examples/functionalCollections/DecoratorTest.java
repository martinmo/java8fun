package examples.functionalCollections;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static examples.functionalCollections.FunctionalList.decorate;

public class DecoratorTest {

    @Test
    public void test() {

        final List<Integer> integers = Arrays.asList(1, 2, 3);
        final FunctionalList<Integer> mapped = decorate(integers).map(i -> i + 1).map(i -> i + 1);
        System.out.println(mapped);

        final FunctionalList<Integer> filter = mapped.filter(i -> i % 2 == 0);
        System.out.println(filter);

    }

}
