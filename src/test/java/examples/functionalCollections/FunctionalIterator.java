package examples.functionalCollections;

import com.google.common.collect.Iterators;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

abstract class FunctionalIterator<E> implements Iterator<E> {

    private FunctionalIterator() {
    }

    protected abstract Iterator<E> getIterator();

    public static <E> FunctionalIterator<E> wrap(Iterator<E> iterator) {
        return new FunctionalIterator<E>() {
            @Override
            protected Iterator getIterator() {
                return iterator;
            }
        };
    }

    public static <E> FunctionalIterator<E> from(Iterable<E> iterable) {
        return wrap(iterable.iterator());
    }

    public <F> FunctionalIterator<F> map(Function<E, F> f) {

        final Iterator<E> underling = getIterator();

        FunctionalIterator<F> i = new FunctionalIterator<F>() {
            @Override
            protected Iterator<F> getIterator() {
                return new Iterator<F>() {
                    @Override
                    public boolean hasNext() {
                        return underling.hasNext();
                    }

                    @Override
                    public F next() {
                        return f.apply(underling.next());
                    }
                };
            }
        };
        return i;
    }

    public FunctionalIterator<E> filter(Predicate<E> f) {
        // nice practice, implement yourself
        return wrap(Iterators.filter(getIterator(), o -> f.test(o)));
    }

    @Override
    public boolean hasNext() {
        return getIterator().hasNext();
    }

    @Override
    public E next() {
        return getIterator().next();
    }

    public List<E> toList() {
        final List<E> list = new LinkedList<>();
        final Iterator<E> underlying = getIterator();
        while (underlying.hasNext()) {
            list.add(underlying.next());
        }
        return list;
    }
}
