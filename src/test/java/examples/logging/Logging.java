package examples.logging;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Ignore
public class Logging {

    @Test
    public void readFile() throws IOException {

        String dir = "C:/Work/wildfly-8.1.0.CR1/standalone/log/server.log";

        FileSystem fs = FileSystems.getDefault();
        Path path = fs.getPath( dir );

        Stream<String> all = Files.lines(path, Charset.forName("utf-8"));

        final Stream<String> warns = all.filter(l -> l.contains("WARN"));

        System.out.println(warns.collect(Collectors.joining("\n")));

    }


}
