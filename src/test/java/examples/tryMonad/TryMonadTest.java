package examples.tryMonad;

import org.junit.Test;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TryMonadTest {

    @Test
    public void test() {

        final Try<Void> alwaysFaulty = Try.tryF(() -> {
            throw new RuntimeException();
        });

        assertThat(alwaysFaulty.isFailure(), is(true));
        assertThat(alwaysFaulty.isSuccess(), is(false));

    }

    @Test(expected = IllegalStateException.class)
    public void canReThrow() {

        final Try<Void> alwaysFaulty = Try.tryF(string -> {
            throw new Exception("ok");
        }, "anyString");

        alwaysFaulty.throwException();
        System.out.println();
    }

    @Test
    public void canParseInt() {
        final String string = "1";
        final Try<Integer> mayBeInt = Try.tryF(() -> Integer.parseInt(string));

        assertThat(mayBeInt.isSuccess(), is(true));
        assertThat(mayBeInt.getValue(), is(1));
    }

    @Test
    public void canMapInt() {
        final String string = "1";
        final Try<Integer> mayBeInt = Try.tryF(() -> Integer.parseInt(string));

        assertThat(mayBeInt.map(i -> i + 1).getValue(), is(2));
    }

    @Test
    public void canMapFailure() {
        final Try<Long> mayBeInt = Try.tryF(() -> {
            throw new Exception();
        });

        assertThat(mayBeInt.map(i -> i + 1).isFailure(), is(true));

        assertThat(Try.success("1").flatMap(s -> Try.failure("fail")).isFailure(), is(true));
    }

    @Test
    public void canFlatMap() {
        final Try<String> mayBeInt = Try.tryF(() -> "1");

        assertThat(mayBeInt.flatMap(s -> Try.success(s + "2")).getValue(), equalTo("12"));
    }

    @Test
    public void canFlatMapFailure() {
        final Try<Long> mayBeInt = Try.tryF(() -> {
            throw new Exception();
        });

        assertThat(mayBeInt.flatMap(i -> Try.success(i + 1)).isFailure(), is(true));
    }

    @Test
    public void canMapToOption() {
        assertThat(Try.failure("fail").toOptional().isPresent(), is(false));

        assertThat(Try.success("ok").toOptional().isPresent(), is(true));
        assertThat(Try.success("ok").toOptional().get(), equalTo("ok"));
    }

    @Test(expected = IllegalStateException.class)
    public void canThrow() {
        final Try<Object> e = Try.failure(new Exception("e"));

        e.throwException();
    }

    @Test
    public void canMapThrowable() {
        assertThat(Try.failure("fail").mapThrowable(t -> "ok").getValue(), equalTo("ok"));

        AtomicBoolean recovered = new AtomicBoolean(false);
        assertThat(Try.failure("fail").getThrowable(t -> {
            recovered.set(true);
        }).isFailure(), is(true));
        assertThat(recovered.get(), is(true));
    }

    @Test
    public void canMapInnerThrowable() {
        assertThat(Try.failure(new Exception("fail")).mapThrowable(t -> t.getMessage()).getValue(), equalTo("fail"));

    }


    // TODO more tests
}
