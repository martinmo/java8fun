package examples.tryMonad;


import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

// http://java.dzone.com/articles/whats-wrong-java-8-part-iv
public abstract class Try<V> {

    private Try() {
    }

    public abstract Boolean isSuccess();

    public abstract Boolean isFailure();

    public abstract void throwException();

    public static <V> Try<V> failure(String message) {
        return new Failure<>(message);
    }

    public static <V> Try<V> failure(String message, Exception e) {
        return new Failure<>(message, e);
    }

    public static <V> Try<V> failure(Exception e) {
        return new Failure<>(e);
    }

    public static <V> Try<V> success(V value) {
        return new Success<>(value);
    }

    public static interface UnsafeFunction<A, B> {
        B apply(A a) throws Exception;
    }

    public static interface UnsafeSupplier<B> {
        B get() throws Exception;
    }

    public static <A, V> Try<V> tryF(UnsafeFunction<A, V> f, A a) {
        try {
            return Try.success(f.apply(a));
        } catch (Exception e) {
            return Try.failure(e);
        }
    }

    public static <V> Try<V> tryF(UnsafeSupplier<V> f) {
        try {
            return Try.success(f.get());
        } catch (Exception e) {
            return Try.failure(e);
        }
    }

    abstract Try<V> getThrowable(Consumer<Throwable> consumer);

    abstract Try<V> mapThrowable(Function<Throwable, V> consumer);

    abstract V getValue();

    abstract <B> Try<B> map(Function<V, B> f);

    abstract <B> Try<B> flatMap(Function<V, Try<B>> f);

    abstract Optional<V> toOptional();


    private static class Failure<V> extends Try<V> {

        private RuntimeException exception;

        public Failure(String message) {
            super();
            this.exception = new IllegalStateException(message);
        }

        public Failure(String message, Exception e) {
            super();
            this.exception = new IllegalStateException(message, e);
        }

        public Failure(Exception e) {
            super();
            // TODO why IllegalStateException ?
            this.exception = new IllegalStateException(e);
        }

        @Override
        public Boolean isSuccess() {
            return false;
        }

        @Override
        public Boolean isFailure() {
            return true;
        }

        @Override
        public void throwException() {
            throw this.exception;
        }

        @Override
        Try<V> getThrowable(Consumer<Throwable> consumer) {
            consumer.accept(exception.getCause());
            return this;
        }

        @Override
        Try<V> mapThrowable(Function<Throwable, V> consumer) {
            final Throwable cause = exception.getCause();
            if (cause == null) {
                return new Success<>(consumer.apply(exception));
            } else {
                return new Success<>(consumer.apply(cause));
            }
        }

        @Override
        V getValue() {
            throw new IllegalStateException("no success");
        }

        @Override
        <B> Try<B> map(Function<V, B> f) {
            return new Failure<>(exception);
        }

        @Override
        <B> Try<B> flatMap(Function<V, Try<B>> f) {
            return new Failure<>(exception);
        }

        @Override
        Optional<V> toOptional() {
            return Optional.empty();
        }

    }

    private static class Success<V> extends Try<V> {

        private V value;

        public Success(V value) {
            super();
            this.value = value;
        }

        @Override
        public Boolean isSuccess() {
            return true;
        }

        @Override
        public Boolean isFailure() {
            return false;
        }

        @Override
        public void throwException() {
            //log.error("Method throwException() called on a Success instance");
        }

        @Override
        Try<V> getThrowable(Consumer<Throwable> consumer) {
            return this;
        }

        @Override
        Try<V> mapThrowable(Function<Throwable, V> consumer) {
            return this;
        }

        @Override
        V getValue() {
            return value;
        }

        @Override
        <B> Try<B> map(Function<V, B> f) {
            return new Success<>(f.apply(value));
        }

        @Override
        <B> Try<B> flatMap(Function<V, Try<B>> f) {
            return f.apply(value);
        }

        @Override
        Optional<V> toOptional() {
            return Optional.of(value);
        }
    }

    // various method such as map an flatMap
}