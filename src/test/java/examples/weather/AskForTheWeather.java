package examples.weather;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import util.TimerRule;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.stream.Stream;

@Ignore
public class AskForTheWeather {

    @Rule
    public TimerRule rule = new TimerRule();

    private final static List<String> cities = Arrays.asList("Berlin", "Hamburg", "Dresden", "Freiburg", "Leipzig", "München", "Köln");


    @Test
    public void askWeather() throws Exception {
        final Future<WeatherResponse> response = WeatherResponse.getFor(cities.get(0));
        System.out.println(response.get());
    }

    @Test
    public void all() {
        final Stream<CompletableFuture<WeatherResponse>> futureResponse = cities.stream().map(WeatherResponse::getFor);
        final Stream<Optional<WeatherResponse>> resultWithFailures = futureResponse.map(f -> {
            try {
                return Optional.of(f.get());
            } catch (Exception e) {
                return Optional.empty();
            }
        });

        final Stream<WeatherResponse> result = resultWithFailures.filter(o -> o.isPresent()).map(o -> o.get());
        result.forEach(System.out::println);
    }

    @Test
    public void allParallel() {
        final Stream<CompletableFuture<WeatherResponse>> map = cities.stream().parallel().map(WeatherResponse::getFor);
        final Stream<Optional<WeatherResponse>> resultWithFailures = map.map(f -> {
            try {
                return Optional.of(f.get());
            } catch (Exception e) {
                return Optional.empty();
            }
        });

        final Stream<WeatherResponse> result = resultWithFailures.filter(o -> o.isPresent()).map(o -> o.get());
        result.forEach(System.out::println);
    }

    @Test
    public void parallelWithFailing() {
        final List<String> cities = Arrays.asList("INvalidCityDoesntExist", "Berlin");
        final Stream<CompletableFuture<WeatherResponse>> map = cities.stream().parallel().map(WeatherResponse::getFor);
        final Stream<Optional<WeatherResponse>> resultWithFailures = map.map(f -> {
            try {
                return Optional.of(f.get());
            } catch (Exception e) {
                System.out.println("failed to get response: " + e.getMessage());
                return Optional.empty();
            }
        });

        final Stream<WeatherResponse> result = resultWithFailures.filter(o -> o.isPresent()).map(o -> o.get());
        result.forEach(System.out::println);
    }


}
