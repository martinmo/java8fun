package examples.weather;

import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public class HTTPAsync {

    public static CompletableFuture<String> get(final String url){
        System.out.println("fetching " + url);
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        CompletableFuture<String> future = new CompletableFuture<>();
        try {
            asyncHttpClient.prepareGet(url).execute(new AsyncCompletionHandler<Response>() {
                @Override
                public Response onCompleted(Response response) throws Exception {
                    System.out.println("finished fetching " + url + " code: " + response.getStatusCode());
                    if(response.getStatusCode() >= 200 && response.getStatusCode() < 300) {
                        future.complete(response.getResponseBody());
                    }else {
                        future.completeExceptionally(new Exception("weather service error: " + response.getStatusText()));
                    }
                    return response;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            future.completeExceptionally(e);
        }
        return future;
    }
}
