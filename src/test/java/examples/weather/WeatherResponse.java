package examples.weather;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

public class WeatherResponse {

    public final String city;
    public final String description;
    public final Double temperature;
    public final Double windSpeed;

    public WeatherResponse(String city, String description, Double temperature, Double windSpeed) {
        this.city = city;
        this.description = description;
        this.temperature = temperature;
        this.windSpeed = windSpeed;
    }

    public static WeatherResponse from(JsonNode node){

        final String city = node.get("name").asText();

        final JsonNode weatherNode = node.get("weather").path(0);
        final String description = weatherNode.get("description").asText();

        final Double temp = node.get("main").get("temp").asDouble();
        final Double windSpeed = node.get("wind").get("speed").asDouble();
        return new WeatherResponse(city, description, temp, windSpeed);
    }

    private static final String URL_TEMPLATE = "http://api.openweathermap.org/data/2.5/weather?q=%s,de&lang=de&units=metric";
    public static CompletableFuture<WeatherResponse> getFor(final String city){
        final String url = String.format(URL_TEMPLATE, city);
        final CompletableFuture<String> future = HTTPAsync.get(url);

        ObjectMapper mapper = new ObjectMapper();

        return future.handle((body,e) -> {
            if(e != null){
                throw new RuntimeException(e);
            }
            try {
                final JsonNode jsonNode = mapper.readValue(body, JsonNode.class);

                // this service does not use HTTP Status Codes to signal failure,
                // but always returns 200 and then sets error code in response body
                if(jsonNode.get("cod").asInt() != 200){
                    throw new AssertionError("service error" + jsonNode.get("message").asText());
                }

                return WeatherResponse.from(jsonNode);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            throw new AssertionError("failed to parse");
        });
    }

    public static Function<String, CompletableFuture<WeatherResponse>> getterFor(){
        return WeatherResponse::getFor;
    }

    @Override
    public String toString() {
        return city + " '"+ description +"', " + temperature + "°C, Wind " + windSpeed + " km/h";
    }
}
