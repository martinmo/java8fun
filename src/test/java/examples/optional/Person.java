package examples.optional;

import java.util.Optional;

public class Person {

    public final String name;

    public final Optional<Address> address;

    public Person(String name, Optional<Address> address) {
        this.name = name;
        this.address = address;
    }

    public static Person of(final String name){
        return new Person(name, Optional.empty());
    }

    public static Person of(final String name, final String phone){
        return new Person(name, Optional.of(new Address(phone)));
    }

    public static class Address {

        public final Optional<String> phone;

        public Address(String phone) {
            this.phone = Optional.of(phone);
        }
        public Address(){
            phone = Optional.empty();
        }
    }

    public static Person randomPerson(){
        return Person.of("John Smith", "+123123");
    }
}
