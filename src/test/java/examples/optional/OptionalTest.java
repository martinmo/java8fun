package examples.optional;

import org.junit.Test;

import java.util.Optional;

public class OptionalTest {

    @Test
    public void phoneNumber() {
        Person person = Person.randomPerson();

        String phone;
        if (person.address.isPresent()) {
            if (person.address.get().phone.isPresent()) {
                phone = person.address.get().phone.get();
            }
        }

        final Optional<String> stringOptional = person.address.flatMap(a -> a.phone);
    }

}
