package examples.optionalUseCases;

import java.util.Optional;

public class NullSavePerson {

    private Optional<NullSafeContactDetails> contactDetails;

    public Optional<NullSafeContactDetails> getContactDetails() {
        return contactDetails;
    }

    public static Optional<NullSavePerson> find() {
        return Optional.empty();
    }

    public void setContactDetails( Optional<NullSafeContactDetails> contactDetails ) {
        this.contactDetails = contactDetails;
    }
}
