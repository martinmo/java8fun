package examples.optionalUseCases;

import java.util.Optional;

public class NullSafeContactDetails {

    private Optional<String> mobilePhone;

    public Optional<String> getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone( Optional<String> mobilePhone ) {
        this.mobilePhone = mobilePhone;
    }

    public static Optional<NullSafeContactDetails> find() {
        return Optional.empty();
    }
}
