package examples.optionalUseCases;


public class ContactDetails {

    private String mobilePhone;

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone( String mobilePhone ) {
        this.mobilePhone = mobilePhone;
    }

    public static ContactDetails find() {
        return null;
    }
}
