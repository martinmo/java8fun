package examples.optionalUseCases;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;

public class OptionalTest {

    @Test
    public void nullCheck() {
        final Person person = Person.find();

        String phone = null;
        if ( person != null && person.getContactDetails() != null ) {
            phone = person.getContactDetails().getMobilePhone();
        }

        if ( phone != null ) {
            sendWelcomeSMS( phone );
        }
    }

    @Test
    public void nullCheck2() {
        final Person person = Person.find();

        if ( person != null && person.getContactDetails() != null ) {
            ContactDetails details = person.getContactDetails();

            if ( details.getMobilePhone() != null ) {
                sendWelcomeSMS( details.getMobilePhone() );
            }
        }
    }

    @Test
    public void optionalCheck() {
        final Optional<NullSavePerson> person = NullSavePerson.find();

        final Optional<String> phone;
        if ( person.isPresent() && person.get().getContactDetails().isPresent() ) {
            phone = person.get().getContactDetails().get().getMobilePhone();
        } else {
            phone = Optional.empty();
        }

        if ( phone.isPresent() ) {
            sendWelcomeSMS( phone.get() );
        }
    }

    @Test
    public void optionalFlatMap() {
        final Optional<NullSavePerson> person = NullSavePerson.find();

        final Optional<String> number = person.flatMap( p -> p.getContactDetails() ).flatMap( c -> c.getMobilePhone() );

        number.ifPresent( n -> sendWelcomeSMS( n ) );
    }

    @Test
    public void optionalFlatMapMethodReference() {
        final Optional<NullSavePerson> person = NullSavePerson.find();

        final Optional<String> number = person.flatMap( NullSavePerson::getContactDetails ).flatMap( NullSafeContactDetails::getMobilePhone );

        number.ifPresent( this::sendWelcomeSMS );
    }

    private void sendWelcomeSMS( final String number ) {
        assertNotNull( number );
    }
}
