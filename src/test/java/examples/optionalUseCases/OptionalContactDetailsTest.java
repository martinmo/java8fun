package examples.optionalUseCases;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;

public class OptionalContactDetailsTest {

    @Test
    public void nullCheck() {
        final ContactDetails contactDetails = ContactDetails.find();

        if ( contactDetails != null && contactDetails.getMobilePhone() != null ) {
            sendWelcomeSMS( contactDetails.getMobilePhone() );
        }
    }

    @Test
    public void optionalCheck() {
        final Optional<NullSafeContactDetails> contactDetails = NullSafeContactDetails.find();

        if ( contactDetails.isPresent() && contactDetails.get().getMobilePhone().isPresent()) {
            sendWelcomeSMS( contactDetails.get().getMobilePhone().get() );
        }
    }

    @Test
    public void optionalFlatMap() {
        final Optional<NullSafeContactDetails> contactDetails = NullSafeContactDetails.find();

        contactDetails.flatMap( c -> c.getMobilePhone() ).ifPresent( number -> sendWelcomeSMS( number ) );
    }

    @Test
    public void optionalFlatMapMethodReference() {
        final Optional<NullSafeContactDetails> contactDetails = NullSafeContactDetails.find();

        contactDetails.flatMap( NullSafeContactDetails::getMobilePhone ).ifPresent( this::sendWelcomeSMS );
    }

    private void sendWelcomeSMS( final String number ) {
        assertNotNull( number );
    }
}
