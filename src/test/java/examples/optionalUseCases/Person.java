package examples.optionalUseCases;

public class Person {

    private ContactDetails contactDetails;

    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    public static Person find() {
        return null;
    }

    public void setContactDetails( ContactDetails contactDetails ) {
        this.contactDetails = contactDetails;
    }
}
