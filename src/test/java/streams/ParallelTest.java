package streams;

import org.junit.Before;
import org.junit.Test;
import util.Computation;
import util.Data;
import util.Timer;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

import static junit.framework.TestCase.assertTrue;

public class ParallelTest {

    @Before
    public void init() {
        System.out.println("INIT ------------");
        Computation.reset();
        System.out.println("cores: " + Runtime.getRuntime().availableProcessors());
        System.out.println("common pool parallelism: " + ForkJoinPool.commonPool().getParallelism());
    }

    @Test
    public void numberOfThreadsSingleThread() {

        final int listSize = 4;
        final int computationTime = 2;

        final Timer timer = new Timer();

        final List<Integer> list = Data.orderedIntegerList(listSize);

        list.stream().forEach(i -> Computation.compute(computationTime));

        assertNear(timer.seconds(), (listSize * computationTime));
    }

    @Test
    public void numberOfThreadsParallel() {

        final int listSize = 20;
        final int computationTime = 2;
        final int cores = Runtime.getRuntime().availableProcessors();

        final Timer timer = new Timer();

        final List<Integer> list = Data.orderedIntegerList(listSize);

        list.stream().parallel().forEach(i -> Computation.compute(computationTime));

        assertNear(timer.seconds(), ((listSize * computationTime) / cores), 25);
    }

    @Test
    public void numberOfThreadsParallelCustomExecutor() throws ExecutionException, InterruptedException {

        final int listSize = 20;
        final int computationTime = 2;
        final int threads = 10;

        final Timer timer = new Timer();

        final List<Integer> list = Data.orderedIntegerList(listSize);

        ForkJoinPool forkJoinPool = new ForkJoinPool(threads);
        forkJoinPool.submit(() ->
                        list.stream().parallel().forEach(i -> Computation.compute(computationTime))
        ).get();

        assertNear(timer.seconds(), ((listSize * computationTime) / threads));
    }

    private void assertNear(long value, long reference) {
        assertNear(value, reference, 10);
    }

    private void assertNear(long value, long reference, int accuracyPercent) {
        final float accuracy = accuracyPercent / 100f;
        assertTrue(reference + " expected but got more " + value, value < reference + reference * (accuracy));
        assertTrue(reference + " expected but got less " + value, value > reference - reference * (accuracy));
    }
}
