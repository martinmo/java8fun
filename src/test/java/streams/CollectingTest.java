package streams;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class CollectingTest {

    public static <T> Collector<T, StringBuilder, String> stringCollector() {
        return Collector.of(StringBuilder::new, StringBuilder::append, (left, right) -> {
            left.append(right);
            return left;
        }, StringBuilder::toString);
    }

    // http://www.jayway.com/2013/11/12/immutable-list-collector-in-java-8/
    public static <T> Collector<T, List<T>, List<T>> toImmutableList() {
        return Collector.of(ArrayList::new, List::add, (left, right) -> {
            left.addAll(right);
            return left;
        }, Collections::unmodifiableList);
    }

    @Test
    public void collect(){
        final String reference = IntStream.rangeClosed(1,10).boxed().map(Object::toString).reduce("", (acc, s) -> acc + s);

        final String result = IntStream.rangeClosed(1, 10).boxed().collect(stringCollector());
        assertThat(result, equalTo(reference));

        final String parallelResult = IntStream.rangeClosed(1, 10).parallel().boxed().collect(stringCollector());
        assertThat(parallelResult, equalTo(reference));
    }
}
