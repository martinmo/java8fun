package streams;

import org.junit.Test;
import util.Data;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class BasicStreamTest {

    private Stream<Integer> ints() {
        return Stream.of(1, 2, 3, 4);
    }

    @Test
    public void construct() {

        final Stream<Integer> of = Stream.of(1, 2, 3, 4);

        final List<Integer> integers = Data.orderedIntegerList(4);
        final Stream<Integer> fromList = integers.stream();
        assertTrue(fromList.count() == 4);

        final Collection<Integer> collection = Data.orderedIntegerList(4);
        final Stream<Integer> fromCollection = collection.stream();

        assertTrue(fromCollection.count() == 4);

        final IntStream range = IntStream.range(1, 11);
        assertTrue(range.count() == 10);
    }

    @Test(expected = IllegalStateException.class)
    public void mutability() {
        final Stream<Integer> of = Stream.of(1, 2, 3, 4);

        of.count();
        of.count();
    }

    @Test
    public void collecting() {
        final List<Integer> list = ints().collect(Collectors.toList());
        final ArrayList<Integer> collect = ints().collect(Collectors.toCollection(ArrayList::new));

        final long count = ints().count();
        final Optional<Integer> max = ints().max(Integer::compare);
    }

    @Test
    public void map() {

        final Stream<String> exp = ints().map(i -> String.valueOf(i));
        final List<String> collected = exp.collect(Collectors.toList());
        assertThat(collected, equalTo(Arrays.asList("1", "2", "3", "4")));
    }

    @Test
    public void filter() {
        final Stream<Integer> filtered = ints().filter(i -> i % 2 == 0);
        final List<Integer> list = filtered.collect(Collectors.toList());
        assertThat(list, equalTo(Arrays.asList(2, 4)));
    }

    @Test
    public void reduce() {
        final Optional<Integer> reduce = ints().reduce((acc, i) -> acc + i);
        assertTrue(reduce.isPresent() && reduce.get() == 1 + 2 + 3 + 4);

        final Optional<String> empty = new LinkedList<String>().stream().reduce((acc, i) -> acc + i);
        assertFalse(empty.isPresent());

        final Integer reduce1 = ints().reduce(0, (acc, i) -> acc + i);
        assertTrue(reduce1 == 1 + 2 + 3 + 4);

        final String reduce2 = ints().reduce("", (String acc, Integer i) -> acc + i, String::concat);
        assertTrue("1234".equals(reduce2));
    }

    @Test
    public void primitive() {
        final IntStream intStream = ints().mapToInt(Integer::new);
        final OptionalInt max = intStream.max();

        assertTrue(max.getAsInt() == 4);
    }

    @Test
    public void find() {
        final Optional<Integer> first = ints().findFirst();
        assertThat(first.get(), equalTo(1));

        final boolean b = ints().anyMatch(i -> i % 2 == 0);

        final Optional<Integer> first1 = ints().filter(i -> i % 2 == 0).findFirst();
        assertThat(first1.get(), equalTo(2));
    }

    private final static Predicate<Integer> even = i -> i % 2 == 0;
    private final Function<Stream<Integer>, Optional<Integer>> findFirstEven = s -> s.filter(even).findFirst();

    @Test
    public void findRefactored() {
        final Optional<Integer> firstEven = findFirstEven.apply(ints());
        assertThat(firstEven.get(), equalTo(2));
    }

    @Test
    public void ordering() {

    }


}
