package streams;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static junit.framework.TestCase.assertTrue;

public class StreamsAndOptionalTest {

    private Stream<Integer> ints() {
        return Stream.of(1, 2, 3, 4);
    }

    @Test
    public void basic() {
        final Optional<Integer> max = ints().max(Integer::compare);
        final Optional<Integer> first = ints().findFirst();
        final Optional<Integer> reduce = ints().reduce((acc, i) -> acc + i);
    }

    @Test
    public void filterMapOptional() {
        final Stream<Optional<Integer>> optionals = Stream.of(Optional.of(1), Optional.<Integer>empty(), Optional.of(2));

        final Stream<Integer> integers = optionals.filter(Optional::isPresent).map(Optional::get);

        assertTrue(integers.count() == 2);
    }

    @Test
    public void flatMapOptional() {
        final List<Optional<Integer>> optionals = Arrays.asList(Optional.of(1), Optional.<Integer>empty(), Optional.of(2));

        final Stream<Integer> objectStream1 = optionals.stream().flatMap(o ->
            o.isPresent() ? Stream.of(o.get()) : Stream.empty()
        );

        assertTrue(objectStream1.count() == 2);


        final Stream<Integer> objectStream2 = optionals.stream().flatMap(o ->
            o.map(i -> Stream.of(i)).orElseGet(() -> Stream.empty())
        );

        assertTrue(objectStream2.count() == 2);
    }

    // http://stackoverflow.com/questions/22725537/using-java-8s-optional-with-streamflatmap
    static <T> Stream<T> streamopt(Optional<T> opt) {
        return opt.map(t -> Stream.of(t))
                .orElseGet(Stream::empty);
    }

    @Test
    public void streamOptional() {
        final List<Optional<Integer>> optionals = Arrays.asList(Optional.of(1), Optional.<Integer>empty(), Optional.of(2));

        final Stream<Integer> flatMapped = optionals.stream().flatMap(StreamsAndOptionalTest::streamopt);

        assertTrue(flatMapped.count() == 2);
    }

}
