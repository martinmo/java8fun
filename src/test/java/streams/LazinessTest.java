package streams;

import org.junit.Test;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static junit.framework.TestCase.assertTrue;

public class LazinessTest {

    @Test
    public void lazinessWithIterator(){
        AtomicInteger done = new AtomicInteger(0);
        final Stream<Integer> ints = Stream.of(1, 2);

        final Stream<Integer> mapped = ints.map(i -> {
            done.incrementAndGet();
            return i;
        });
        assertTrue(done.get() == 0);

        final Iterator<Integer> iterator = mapped.iterator();
        iterator.next();
        assertTrue(done.get() == 1);

        iterator.next();
        assertTrue(done.get() == 2);
    }

    @Test
    public void laziness(){
        AtomicInteger done = new AtomicInteger(0);
        final Stream<Integer> ints = Stream.of(1, 2);

        final Stream<Integer> mapped = ints.map(i -> {
            done.incrementAndGet();
            return i;
        });
        assertTrue(done.get() == 0);

        mapped.collect(Collectors.toList());
        assertTrue(done.get() == 2);
    }

    @Test
    public void limit(){
        AtomicInteger done = new AtomicInteger(0);
        final Stream<Integer> ints = Stream.of(1, 2);

        final Stream<Integer> mapped = ints.map(i -> {
            done.incrementAndGet();
            return i;
        });

        mapped.limit(1).collect(Collectors.toList());
        assertTrue(done.get() == 1);
    }

    @Test
    public void skip(){
        AtomicInteger done = new AtomicInteger(0);
        final Stream<Integer> ints = Stream.of(1, 2);

        final Stream<Integer> mapped = ints.map(i -> {
            done.incrementAndGet();
            return i;
        });

        mapped.skip(1).collect(Collectors.toList());
        assertTrue(done.get() == 2);
    }


}
