package streams;

import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GroupingTest {


    @Test
    public void group(){
        final Map<Boolean, List<Integer>> evenOrNot = IntStream.rangeClosed(1, 10)
                .boxed()
                .collect(Collectors.groupingBy(i -> i % 2 == 0));

        assertThat(evenOrNot.get(true).size(), is(5));
        assertThat(evenOrNot.get(false).size(), is(5));
    }


}
